<center>Отчёт по лабораторной работе №5</center>
================================================
<center>по дисциплине "Современные операционные системы"</center>
-----------------------------------------------------------------

## <center>Тема: использование жёстких и символических ссылок</center> ##

## <center>Выполнила: группа М091601(72) Картавая И. И. </center> ##

### <center>1. Постановка задачи</center> ###

Использовать жёсткие и символические ссылки.

#### <center>Создание жёстких ссылок</center> ####

**1. Создаём исходный файл для жёсткой ссылки**

	us-virtual-machine ~/lab_5/src $ touch hard_src_text.txt
	us-virtual-machine ~/lab_5/src $ nano hard_src_text.txt 
	us-virtual-machine ~/lab_5/src $ cat hard_src_text.txt 
	Hard Link Source Text
	us-virtual-machine~/lab_5/src $ ls -i
	2339 hard_src_text.txt

**2. Создаём жёсткую ссылку на файл в другой папке**

	us-virtual-machine ~/lab_5/dest $ ln ../src/hard_src_text.txt hard_dest_text.txt
	us-virtual-machine ~/lab_5/dest $ cat hard_dest_text.txt 
	Hard Link Source Text
	us-virtual-machine ~/lab_5/dest $ ls -i
	2339 hard_dest_text.txt

**3. Перемещаем исходный файл в другую папку**

	us-virtual-machine ~/lab_5/src $ mv hard_src_text.txt ../
	us-virtual-machine ~/lab_5/src $ cd ..
	us-virtual-machine ~/lab_5 $ ls -i
	2339 hard_src_text.txt

**4. Проверяем доступность файла по жёсткой ссылке**

	us-virtual-machine ~/lab_5/dest $ cat hard_dest_text.txt
	Hard Link Source Text

Файл и жёсткая ссылка на файл имеют одинковые номера inode'ов, перемещение 
исходного файла не влияет на его доступность по ссылке

#### <center>Создание символических ссылок</center> ####

**Задание 1**

**1. Символические ссылки в папке /bin**

	us-virtual-machine /bin $ ls -l
	...
	lrwxrwxrwx 1 root root       6 февр. 12  2016 bzcmp -> bzdiff
	-rwxr-xr-x 1 root root    2140 окт.  21  2013 bzdiff
	lrwxrwxrwx 1 root root       6 февр. 12  2016 bzegrep -> bzgrep
	-rwxr-xr-x 1 root root    4877 окт.  21  2013 bzexe
	lrwxrwxrwx 1 root root       6 февр. 12  2016 bzfgrep -> bzgrep
	-rwxr-xr-x 1 root root    3642 окт.  21  2013 bzgrep
	-rwxr-xr-x 1 root root   31152 окт.  21  2013 bzip2
	-rwxr-xr-x 1 root root   14480 окт.  21  2013 bzip2recover
	lrwxrwxrwx 1 root root       6 февр. 12  2016 bzless -> bzmore
	...
	lrwxrwxrwx 1 root root       8 февр. 12  2016 dnsdomainname -> hostname
	lrwxrwxrwx 1 root root       8 февр. 12  2016 domainname -> hostname
	...
	lrwxrwxrwx 1 root root       8 февр. 12  2016 lessfile -> lesspipe
	...
	lrwxrwxrwx 1 root root       4 февр. 12  2016 lsmod -> kmod
	...
	lrwxrwxrwx 1 root root      20 февр. 12  2016 mt -> /etc/alternatives/mt
	-rwxr-xr-x 1 root root   68760 февр. 18  2016 mt-gnu
	-rwxr-xr-x 1 root root  122088 марта 10  2016 mv
	-rwxr-xr-x 1 root root  192008 окт.   1  2012 nano
	lrwxrwxrwx 1 root root      20 февр. 12  2016 nc -> /etc/alternatives/nc
	-rwxr-xr-x 1 root root   31248 дек.   4  2012 nc.openbsd
	lrwxrwxrwx 1 root root      24 февр. 12  2016 netcat -> /etc/alternatives/netcat
	-rwxr-xr-x 1 root root  119624 авг.   5  2014 netstat
	lrwxrwxrwx 1 root root       8 февр. 12  2016 nisdomainname -> hostname
	...

**Задание 2**

**2.1. Создаём исходный файл для символической ссылки**

	us-virtual-machine ~/lab_5/src $ touch symb_src_text.txt
	us-virtual-machine ~/lab_5/src $ nano symb_src_text.txt 
	us-virtual-machine ~/lab_5/src $ cat symb_src_text.txt 
	Source Text File
	us-virtual-machine ~/lab_5/src $ ls -i
	2278 symb_src_text.txt

**2.2. Создаём символическую ссылку на файл**
	
	us-virtual-machine ~/lab_5/dest $ ln -s ../src/symb_src_text.txt symb_dest_text.txt
	us-virtual-machine~/lab_5/dest $ cat symb_dest_text.txt 
	Source Text File
	us-virtual-machine ~/lab_5/dest $ ls -l
	lrwxrwxrwx 1 us us 15 дек. 13 18:37 symb_dest_text.txt -> ../src/symb_src_text.txt
	us-virtual-machine ~/lab_5/dest $  ls -i
	983 symb_dest_text.txt

Файл и символическая ссылка на файл имеют разные номера inode'ов.

**2.3. Перемещаем исходный файл в другую папку**

	us-virtual-machine ~/lab_5 $ mkdir other_path
	us-virtual-machine ~/lab_5 $ mv src/symb_src_text.txt other_path/symb_src_text.txt
	
**2.4. Проверяем доступность файла по символической ссылке**

	us-virtual-machine ~/lab_5/dest $ ls -l
	lrwxrwxrwx 1 us us 15 дек. 13 18:37 symb_dest_text.txt -> ../src/symb_src_text.txt
	us-virtual-machine ~/lab_5/dest $ cat symb_dest_text.txt 
	cat: symb_dest_text.txt: Нет такого файла или каталога

Файл по ссылке отсутствует, так как изменился относительный путь к нему.

**Задание 3**

**3.1. Создаём исходные файлы для символических ссылок**

	us-virtual-machine ~/lab_5/src $ touch symb_src_text1.txt
	us-virtual-machine ~/lab_5/src $ nano symb_src_text1.txt
	aus-virtual-machine ~/lab_5/src $ cat symb_src_text1.txt 
	Source Text 1

	us-virtual-machine ~/lab_5/src $ touch symb_src_text2.txt
	us-virtual-machine ~/lab_5/src $ nano symb_src_text2.txt
	us-virtual-machine ~/lab_5/src $ cat symb_src_text2.txt 
	Source Text 2

**3.2. Создаём символические ссылки на файлы**

	us-virtual-machine ~/lab_5/src $ ln -s symb_src_text1.txt symb_dest_text1.txt
	us-virtual-machine ~/lab_5/src $ ln -s symb_src_text2.txt symb_dest_text2.txt
	us-virtual-machine ~/lab_5/src $ ls -l
	lrwxrwxrwx 1 us us 15 дек. 13 20:29 symb_dest_text1.txt -> symb_src_text1.txt
	lrwxrwxrwx 1 us us 15 дек. 13 20:29 symb_dest_text2.txt -> symb_src_text2.txt
	-rw-r--r-- 1 us us 16 дек. 13 20:28 symb_src_text1.txt
	-rw-r--r-- 1 us us 16 дек. 13 20:28 symb_src_text2.txt

**3.3. Перемещаем папку в другое место**

	us-virtual-machine ~/lab_5 $ mv src dest/src
	us-virtual-machine ~/lab_5 $ cd dest/src
	us-virtual-machine ~/lab_5/dest/src $ ls -l
	lrwxrwxrwx 1 us us 15 дек. 13 20:29 symb_dest_text1.txt -> symb_src_text1.txt
	lrwxrwxrwx 1 us us 15 дек. 13 20:29 symb_dest_text2.txt -> symb_src_text2.txt
	-rw-r--r-- 1 us us 16 дек. 13 20:28 symb_src_text1.txt
	-rw-r--r-- 1 us us 16 дек. 13 20:28 symb_src_text2.txt

**3.4. Проверяем доступность файлов по символическим ссылкам**

	us-virtual-machine ~/lab_5/dest/src $ cat symb_dest_text1.txt 
	Source Text 1
	us-virtual-machine ~/lab_5/dest/src $ cat symb_dest_text2.txt 
	Source Text 2

Файлы по символическим ссылкам доступны, т.к. относительные пути к исходным
файлам не изменились.