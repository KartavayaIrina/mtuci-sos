<center>Отчёт по лабораторной работе №3</center>
================================================
<center>по дисциплине "Современные операционные системы"</center>
-----------------------------------------------------------------

## <center>Тема: использование git-репозитория</center> ##

## <center>Выполнила: студентка группы М091601(72) Картавая И.</center> ##

### <center>1. Постановка задачи</center> ###

Создать git-репозиторий и использовать его для хранения лабораторных работ.<br>
Добавить синхронизацию с удалённым хранилищем GitLab.

### <center>2. Практическая часть</center> ###

**1. Создаём папку для локального git-репозитория**

	us-virtual-machine us # mkdir /home/us/sos
	us-virtual-machine us # cd  /home/us/sos


**2. Создаём пустой git-репозиторий**

	us-virtual-machine sos # git init
	Initialized empty Git repository in /home/us/sos/.git/


**3. Подключаем удалённый git-репозиторий GitLab**

	us-virtual-machine sos 
	#  git remote add GitLab https://gitlab.com/KartavayaIrina/sos.git

	
	us-virtual-machine sos # git remote
	GitLab

**4. Добавляем в репозиторий отчет по первой лаборатоной работе**

	us-virtual-machine sos # git add .
	
	us-virtual-machine sos # git status
	# On branch master
	#
	# Initial commit
	#
	# Untracked files:
	#   (use "git add <file>..." to include in what will be committed)
	#
	#	lab1.docx
	nothing added to commit but untracked files present (use "git add" to track)


**5. Выполняем первый коммит**

	us-virtual-machine sos # git commit -m "add lab1"
	
	us-virtual-machine sos # git log
	commit 0ef7aa998c7854473d48a47bed4a3802d36dc594
	Author: root <root@us-virtual-machine.(none)>
	Date:   Wed Sep 28 11:34:07 2016 +0400

		add lab1


**6. Создаём ветку для отчёта лабораторной работы №2 Lab2**

	us-virtual-machine sos # git branch Lab2
	us-virtual-machine sos # git checkout Lab2
	Switched to branch 'Lab2'


**7. Добавляем файлы отчёта лабораторной работы №1**

	us-virtual-machine sos # git add "lab2.docx"
	us-virtual-machine sos # git status
	# On branch Lab2
	# Changes to be committed:
	#   (use "git reset HEAD <file>..." to unstage)
	#
	#	new file:   lab2.docx
	#

**8. Выполняем коммит с отчётом лабораторной работы №2**

	us-virtual-machine sos # git commit -m "add lab2"
	[Lab2 e412eec] add lab2
	 1 file changed, 92 insertions(+)
	 create mode 100644 lab2.docx

	us-virtual-machine sos # git log
	commit e412eecfb42523d3bf0c35b3c6638bc230246656
	Author: KartavayaIrina <Kartavaya_i@mail.ru>
	Date:   Wed Sep 28 12:03:57 2016 +0400

		add lab2

	commit 0ef7aa998c7854473d48a47bed4a3802d36dc594
	Author: root <root@us-virtual-machine.(none)>
	Date:   Wed Sep 28 11:34:07 2016 +0400

		add lab1


**9. Выполняем слияние веток master и Lab2**
	
	us-virtual-machine sos # git checkout master
	Switched to branch 'master'
	us-virtual-machine sos # git merge --no-commit --no-ff --log Lab2
	Automatic merge went well; stopped before committing as requested
	us-virtual-machine sos # git status
	# On branch master
	# Changes to be committed:
	#
	#	new file:   lab2.docx
	#
	
	us-virtual-machine sos # git commit -m "merge master and Lab2"
	[master a373126] merge master and Lab2
	
	us-virtual-machine sos # git log
	commit a37312680d8fdf13b6d6851ca443086a8c72b2dc
	Merge: 0ef7aa9 e412eec
	Author: KartavayaIrina <Kartavaya_i@mail.ru>
	Date:   Wed Sep 28 12:12:34 2016 +0400

		merge master and Lab2

	commit e412eecfb42523d3bf0c35b3c6638bc230246656
	Author: KartavayaIrina <Kartavaya_i@mail.ru>
	Date:   Wed Sep 28 12:03:57 2016 +0400

		add lab2

	commit 0ef7aa998c7854473d48a47bed4a3802d36dc594
	Author: root <root@us-virtual-machine.(none)>
	Date:   Wed Sep 28 11:34:07 2016 +0400

		add lab1

	

**10. Присваиваем коммиту метку**

	us-virtual-machine sos # git tag Lab2
	
	us-virtual-machine sos # git tag 
	Lab2


**11. Удаляем ненужную ветку Lab_1**

	us-virtual-machine sos # git branch --delete Lab2
	Deleted branch Lab2 (was e412eec).


**12. Проверяем список коммитов**

	us-virtual-machine sos # git log --graph
	*   commit a37312680d8fdf13b6d6851ca443086a8c72b2dc
	|\  Merge: 0ef7aa9 e412eec
	| | Author: KartavayaIrina <Kartavaya_i@mail.ru>
	| | Date:   Wed Sep 28 12:12:34 2016 +0400
	| | 
	| |     merge master and Lab2
	| |   
	| * commit e412eecfb42523d3bf0c35b3c6638bc230246656
	|/  Author: KartavayaIrina <Kartavaya_i@mail.ru>
	|   Date:   Wed Sep 28 12:03:57 2016 +0400
	|   
	|       add lab2
	|  
	* commit 0ef7aa998c7854473d48a47bed4a3802d36dc594
	  Author: root <root@us-virtual-machine.(none)>
	  Date:   Wed Sep 28 11:34:07 2016 +0400
	  
		  add lab1

	
**13. Отправляем данные на удалённый git-репозиторий GitLab**

	us-virtual-machine sos # git push -f GitLab master
	Username for 'https://gitlab.com': KartavayaIrina
	Password for 'https://KartavayaIrina@gitlab.com': 
	To https://gitlab.com/KartavayaIrina/mtuci-sos.git
	 * [new branch]      master -> master

